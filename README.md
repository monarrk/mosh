# mosh
datamoshing: simplified

### building
this project requires the following dependencies:
- libpng
- libsndfile

this is tested with the Tiny C Compiler (TCC) but it should work with other compilers fine

to build, simply run `make`.


### usage
to use, run `mosh [FILE] --type [TYPE]` where `[FILE]` is the file you wish to mosh, and `[TYPE]` is the filetype (defaults to `any`). You can find supported types with the `--types` flag.
