CC = tcc
CFLAGS = -Wall -Werror
LINK = -lpng -lsndfile

PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

all: mosh

mosh: mosh.c
	${CC} -o mosh ${CFLAGS} ${LINK} mosh.c

clean:
	rm -f mosh *.png *.wav *.jpg

run: mosh.c
	tcc ${LINK} ${CFLAGS} -run mosh.c

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f mosh ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/mosh
	cp -f man/mosh.1 ${DESTDIR}${MANPREFIX}/man1/mosh.1
