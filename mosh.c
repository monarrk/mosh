#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <png.h>
#include <sndfile.h>

#define matches(a, b) (strcmp((a), (b)) == 0)

// what to do to each byte
// so far XOR works the best (for PNG at least)
#define METHOD ^=

// A PNG image
typedef struct {
	int rows;
	int cols;
	int** rmap;
	int** gmap;
	int** bmap;
	int** amap;
} PNGImage;

// func prototypes
void mosh_any(FILE* f, int amount);
void mosh_png(FILE* f, int amount);
void mosh_wav(SNDFILE* f, int amount);

// mosh data of any type completely randomly
void mosh_any(FILE* f, int amount) {
	int ch;
	while ((ch = fgetc(f)) != EOF) {
		int r = rand();
		if (!(r % amount)) {
			// distort the char
			ch METHOD r;
		}

		// print out the char
		char* s = (char*)ch;
		printf("%c", s);
	}
}

// mosh a wave file
// TODO finish
#define BUFFER_LEN 4096
void mosh_wav(SNDFILE* f, int amount) {
	int readcount;
	static int buffer[BUFFER_LEN];

	int prev = rand();	
	while ((readcount = sf_read_int(f, buffer, BUFFER_LEN)) > 0) {
		//readcount METHOD prev METHOD prev;

		printf("%c", readcount);

		prev = readcount;
	}
}

// mosh a PNG file
// a lot of this code is from https://gist.github.com/abforce/2a4dbdeb47d4e6bcaf79de38380a13b9
void mosh_png(FILE* f, int amount) {
	PNGImage* image = malloc(sizeof(PNGImage));
	png_byte color_type;
	png_bytep* row_pointers;
	png_structp png_ptr;
	png_infop info_ptr;
	int i, j;

	// 8 is the maximum size that can be checked char header[8];
	char header[8];

	fread(header, 1, 8, f);
	if (png_sig_cmp(header, 0, 8)) {
		fprintf(stderr, "[read_png_file] File is not recognized as a PNG file");
		return;
	}

	/* initialize stuff */
	png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

	if (!png_ptr) {
		fprintf(stderr, "[read_png_file] png_create_read_struct failed");
		return;
	}

	info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr) {
		fprintf(stderr, "[read_png_file] png_create_info_struct failed");
		return;
	}

	if (setjmp(png_jmpbuf(png_ptr))) {
		fprintf(stderr, "[read_png_file] Error during init_io");
	}

	png_init_io(png_ptr, f);
	png_set_sig_bytes(png_ptr, 8);

	png_read_info(png_ptr, info_ptr);

	image->cols = png_get_image_width(png_ptr, info_ptr);
	image->rows = png_get_image_height(png_ptr, info_ptr);

	color_type = png_get_color_type(png_ptr, info_ptr);
	if (color_type != PNG_COLOR_TYPE_RGB && color_type != PNG_COLOR_TYPE_RGBA) {
		fprintf(stderr, "[read_png_file] Only RGB and RGBA PNGs are supported");
		return;
	}

	// if they don't have an alpha, then add filler
	if (color_type != PNG_COLOR_TYPE_RGBA) png_set_filler(png_ptr, 0xFF, PNG_FILLER_AFTER);
	
	// update
	png_read_update_info(png_ptr, info_ptr);

	/* read file */
	if (setjmp(png_jmpbuf(png_ptr))) {
		fprintf(stderr, "[read_png_file] Error during read_image");
		return;
	}

	/* memory allocation */
	row_pointers = (png_bytep*)malloc(sizeof(png_bytep) * image->rows);
	for (i = 0; i < image->rows; i += 1) {
		row_pointers[i] = (png_byte*)malloc(png_get_rowbytes(png_ptr, info_ptr));
	}

	image->rmap = malloc(image->rows * sizeof(int*));
	for (i = 0; i < image->rows; i += 1)
		(image->rmap)[i] = malloc(image->cols * sizeof(int));

	image->gmap = malloc(image->rows * sizeof(int*));
	for (i = 0; i < image->rows; i += 1)
		(image->gmap)[i] = malloc(image->cols * sizeof(int));

	image->bmap = malloc(image->rows * sizeof(int*));
	for (i = 0; i < image->rows; i += 1)
		(image->bmap)[i] = malloc(image->cols * sizeof(int));

	image->amap = malloc(image->rows * sizeof(int*));
	for (i = 0; i < image->rows; i += 1)
		(image->amap)[i] = malloc(image->cols * sizeof(int));

	png_read_image(png_ptr, row_pointers);
	fclose(f);

	for (i = 0; i < image->rows; i += 1) {
		png_byte* row = row_pointers[i];
		for (j = 0; j < image->cols; j += 1) {
			png_byte* ptr = &(row[j * 4]);

			image->rmap[i][j] = ptr[0];
			image->gmap[i][j] = ptr[1];
			image->bmap[i][j] = ptr[2];
			image->amap[i][j] = ptr[3];
		}
	}

	/* clean up */
	for (i = 0; i < image->rows; i += 1) {
		free(row_pointers[i]);
	}
	free(row_pointers);

	// write image
	/*  Magic number writing */
	printf("P6\n");

	/* Dimensions */
	printf("%d %d \n", image->cols, image->rows);

	/* Max val */
	printf("%d\n", 255);

	/* Pixel values */
	int last_rmap = rand();
	int last_bmap = rand();
	int last_gmap = rand();

	// go over each pixel and distort it
	for (i = 0; i < image->rows; i += 1) {
		for (j = 0; j < image->cols; j += 1) {
			// get the RGB values
			int rmap = image->rmap[i][j];
			int bmap = image->bmap[i][j];
			int gmap = image->gmap[i][j];
			int amap = image->amap[i][j];

			// distort them according to the last pixel and the amount var
			rmap METHOD last_rmap METHOD amount;
			bmap METHOD last_bmap METHOD amount;
			gmap METHOD last_gmap METHOD amount;
			// don't mosh the alpha
			// amap METHOD last_amap METHOD amount;
			
			// print them out
			printf("%c", rmap);
			printf("%c", gmap);
			printf("%c", bmap);
			printf("%c", amap);

			// set the last pixels
			last_rmap = rmap;
			last_bmap = bmap;
			last_gmap = gmap;
		}
	}
}

// print a help message and return -1
int help() {
	printf("Usage: mosh [FILE] [OPTIONS]
	
OPTIONS:
    --type    declare the file type to parse headers correctly
    --amount  set the \"amount\" of moshing to do. this effect varies from type to type
                - for any, 50 is recommended
		- for png, 4 is recommended
    --types   print the available types

if you experience bugs, please report them at https://gitlab.com/monarrk/mosh/-/issues.\n");
	return 0;
}

int main(int argc, char** argv) {
	// needs at least one argument
	if (argc < 2) {
		return help();
	}

	// options for running which we'll extract from the arguments
	char* file = NULL;
	char* type = NULL;
	int amount = 50;
	
	// iterate arguments
	for (int i = 1; i < argc; i++) {
		if (matches(argv[i], "--help")) {
			return help();
		} else if (matches(argv[i], "--type")) {
			// get the next arg
			if ((i + 1) < argc) {
				i++;
				type = argv[i];
			} else {
				return help();
			}
		} else if (matches(argv[i], "--types")) {
			printf("Available types: wav, png, any\n");
			return 0;
		} else if (matches(argv[i], "--amount")) {
			if ((i + 1) < argc) {
				i++;
				amount = atoi(argv[i]);
			} else {
				return help();
			}
		} else {
			file = argv[i];;
		}
	}

	// if no file was provided, abort
	if (!file) {
		return help();
	}

	// if no type was provided, assume "any"
	if (!type) {
		type = "any";
	}

	// open the target file
	FILE* f = fopen(file, "rb");
	if (!f) {
		printf("Error opening file. Aborting\n");
		return -1;
	}

	// mosh according to the provided type
	if (matches(type, "wav")) {
		SF_INFO	sfinfo;
		memset(&sfinfo, 0, sizeof(sfinfo));

		SNDFILE* sf = sf_open(file, SFM_READ, &sfinfo);
		if (!sf) {
			printf("Failed to open sndfile. Aborting.\n");
			return -1;
		}

		sfinfo.format = (SF_FORMAT_WAV | SF_FORMAT_PCM_24);
		sfinfo.channels = 2;
		sfinfo.samplerate = 44100;

		mosh_wav(sf, amount);
		sf_close(sf);
	} else if (matches(type, "any")) {
		mosh_any(f, amount);
	} else if (matches(type, "png")) {
		mosh_png(f, amount);
	} else {
		printf("Type '%s' not recognized. Aborting\n", type);
	}

	return 0;
}
